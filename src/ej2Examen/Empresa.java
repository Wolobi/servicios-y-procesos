package ej2Examen;

import java.util.Random;

public class Empresa {

	public static int NUM_MAX_FOLLETOS = 20;

	public static void main(String[] args) throws InterruptedException {

		System.out.println("�Cuantos repatidores trabajaran hoy?");
		int num_repartidores = utilidades.Entrada.entero();

		int daremosPrioridad = new Random().nextInt(num_repartidores);

		for (int i = 0; i < num_repartidores; i++) {
			Repartidor repartidor = new Repartidor("Repartidor " + (i + 1), new Random().nextInt(NUM_MAX_FOLLETOS) + 1);
			if (i == daremosPrioridad) {
				System.out.println("Aumentando priodad al repartidor " + repartidor.nombre);
				repartidor.setPriority(Thread.MAX_PRIORITY);
			}
			repartidor.start();

		}
	

	}

}
