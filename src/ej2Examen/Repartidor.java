package ej2Examen;

public class Repartidor extends Thread {
	String nombre;
	int num_folletos;

	public Repartidor(String nombre, int num_folletos) {
		super();
		this.num_folletos = num_folletos;
		this.nombre = nombre;
	}

	@Override
	public void run() {
		System.out.println(nombre + " saliendo");
		try {
			Thread.sleep(1000);
			long tiempo = 0;
			int total_folletos = num_folletos;
			for (int i = total_folletos; i > 0; i--) {
				long inicio = System.currentTimeMillis();
				System.out.println(nombre + " repartiendo folledo");
				Thread.sleep(200);
				long fin = System.currentTimeMillis();
				tiempo += fin - inicio;
			}

			System.out.println("El repartidor " + nombre + " h� tardado " + tiempo / 100
					+ " minutos en repartir todos los " + num_folletos + " folletos");

		} catch (InterruptedException e1) {
		}
	}

}
