package ej3_1;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import utilidades.Entrada;

public class Cliente {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		String host = "localhost";
		int nPuerto = 5000; // puerto remoto
		Socket cliente = null;

		String entrada;
		int contador = 1;

		String nick = "fafa";
		//Scanner sc = new Scanner(System.in);
		cliente = new Socket(host, nPuerto); // throws IOException &

		ObjectOutputStream objetoSaliente = new ObjectOutputStream(cliente.getOutputStream());
		new ClienteChatEntrante(cliente);//ABRIMOS HILO MENSAJE ENTRANTE
		while (true) {
			System.out.println("Introduce cadena " + contador + ":");

			entrada = Entrada.cadena();

			Mensaje mensaje = new Mensaje(entrada, nick);

			objetoSaliente.writeObject(mensaje);

			if (entrada.equalsIgnoreCase("fin")) {

				objetoSaliente.close();
			} else {
				contador++;
			}

		}

	}

}
