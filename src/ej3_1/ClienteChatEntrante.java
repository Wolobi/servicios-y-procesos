package ej3_1;

import java.io.ObjectInputStream;
import java.net.Socket;

public class ClienteChatEntrante implements Runnable {

	
	
	private Socket serverConectado;
	
	public ClienteChatEntrante(Socket clienteConectado) {
		this.serverConectado = clienteConectado;
		Thread t = new Thread(this, "Nuevo Thread");
		t.start();
	}

	@Override
	public void run() {
		try {
			ObjectInputStream is = new ObjectInputStream(serverConectado.getInputStream());
			
			while (true) {

				Mensaje m = (Mensaje) is.readObject();
				System.out.println(m.getNombreUsuario() + ": " + m.getMensaje());
			}
			
		} catch (Exception e) {
			System.out.println("asd asd " + e.getMessage());
		}

		System.out.println("Finalizando hilo");

	}

}
