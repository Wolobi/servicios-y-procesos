package ej3_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Observable;
import java.util.Observer;

public class ClienteConectado extends Thread implements Observer {

	Socket clienteConectado;
	ObjectInputStream ois;
	ObjectOutputStream oos;
	Mensaje mensajes;

	public ClienteConectado(Socket clienteConectado, Mensaje mensajes) {
		this.mensajes = mensajes;
		this.clienteConectado = clienteConectado;
		try {
			this.ois = new ObjectInputStream(clienteConectado.getInputStream());
			this.oos = new ObjectOutputStream(clienteConectado.getOutputStream());
		} catch (IOException ex) {
			System.out.println("Fallo en constructor del hilo: ");
		}
		System.out.println("Constructor del hilo finalizado");
	}

	public void run() {
		Mensaje m;

		mensajes.addObserver(this);

		boolean conectado = true;
		try {
		while (conectado) {


				m = (Mensaje) ois.readObject();
				mensajes.setMensaje(m);

//				m.setMensaje(m.getMensaje()+" Num de clientes conectados: "+ 
//				Servidor.alCLientes.size());
//				for (int i = 0; i < Servidor.alCLientes.size(); i++) {
//					if (!clienteConectado.isClosed()) {
//						try {
//							enviarMensaje(Servidor.alCLientes.get(i).oos,m);
//						} catch (IOException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//					}else {
//						Servidor.alCLientes.remove(i);
//						
//					}
				// Servidor.alCLientes.get(i).


				if (m.getMensaje().equalsIgnoreCase("fin")) {
					conectado = false;
				}
			
		}
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			try {
				ois.close();
				System.out.println("Cerrando canuto de entrada");
			} catch (IOException e1) {
				System.out.println("Fallo al cerrar canuto de entrada");
			}
			try {
				oos.close();
				System.out.println("Cerrando canuto de salida");
			} catch (IOException e1) {
				System.out.println("Fallo al cerrar canuto de salida");
			}finally {
				try {
					mensajes.deleteObserver(this);
					clienteConectado.close();
					System.out.println("Cerrando socket");
				} catch (IOException a) {
					System.out.println("Fallo al cerrar socket");
				}
			}
		}

		// el cliente envía un mensaje
		// System.out.println("Recibiendo: "+flujoEntrada.readUTF()); // IOException
		// System.out.println(m.getNombreUsuario() + ": " + m.getMensaje());

		// cerramos streams y sockets

//			}

	}



	private void enviarMensaje(ObjectOutputStream oos, Mensaje m) throws IOException {
		oos.writeObject(m);
	}

	@Override
	public void update(Observable o, Object arg) {
		System.out.println("CAMBIO EN HILO");
		try {
			oos.writeObject((Mensaje)arg);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
