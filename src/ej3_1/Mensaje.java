package ej3_1;

import java.io.Serializable;
import java.util.Observable;

public class Mensaje extends Observable implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6303583365935494508L;
	private String mensaje;
	private String nombreUsuario;
	private Mensaje m;

	public Mensaje(String mensaje, String nombreUsuario) {
		super();
		this.mensaje = mensaje;
		this.nombreUsuario = nombreUsuario;
	}

	public Mensaje() {
	}

	public String getMensaje() {
		return mensaje;
	}
	
	public Mensaje getM() {
		return m;
	}

	public void setMensaje(Mensaje mens) {
		this.m = mens;
		// Indica que el mensaje ha cambiado
		this.setChanged();
		// Notifica a los observadores que el mensaje ha cambiado y se lo pasa
		// (Internamente notifyObservers llama al metodo update del observador)
		this.notifyObservers(this.getM());
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

}
