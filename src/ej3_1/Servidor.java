package ej3_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Servidor {

	//public static ArrayList<ClienteConectado> alCLientes;

	public static void main(String[] args){
		//alCLientes = new ArrayList<>();
		Mensaje mensajes = new Mensaje();

		
		int nPuerto = 5000; // puerto local
		ServerSocket servidor = null;
		Socket clienteConectado = null;
		
		
        try {
    		servidor = new ServerSocket(nPuerto); // throws IOException

        	
		//servidor = new ServerSocket(nPuerto); // throws IOException


		while (true) {
			System.out.println("Esperando al cliente....");
			clienteConectado = servidor.accept(); // throws IOException
			//new ClienteConectado(clienteConectado);
//			ObjectInputStream ois = new ObjectInputStream(clienteConectado.getInputStream());
//			ObjectOutputStream oos = new ObjectOutputStream(clienteConectado.getOutputStream());
			
			ClienteConectado conexionCliente = new ClienteConectado(clienteConectado, mensajes);
			
			// creamos flujo de entrada del cliente
			//alCLientes.add(conexionCliente);
			conexionCliente.start();

		}
        }catch (Exception e) {
			// TODO: handle exception
		}finally{
            try {
            	clienteConectado.close();
                servidor.close();
            } catch (IOException ex) {
            }
        }
	}

}
