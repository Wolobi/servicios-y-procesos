package ejemploSocketStream_Cliente_Servidor;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import utilidades.Entrada;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.awt.event.ActionEvent;

public class Chat extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Chat frame = new Chat();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Chat() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonEnviar();
			}
		});
		btnNewButton.setBounds(335, 228, 89, 23);
		contentPane.add(btnNewButton);
		
		textField = new JTextField();
		textField.setBounds(10, 229, 315, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 414, 208);
		contentPane.add(scrollPane);
		
		JTextArea textArea_chat = new JTextArea();
		scrollPane.setViewportView(textArea_chat);
		textArea_chat.setText("h");
		new IniciarServidor().start();
	}

	
	
	protected void botonEnviar() {
		// TODO Auto-generated method stub
		
	}
}

class IniciarServidor extends Thread {
	public void run() {
		System.out.println("Hola desde el hilo creado!");
		startServer();
	}
	
private void startServer() {
		
        try {
            System.out.println ("Creación del socket servidor");
            ServerSocket serverSocket = new ServerSocket();
            System.out.println ("Realización del bind");
            InetSocketAddress iSA = new InetSocketAddress("localhost",5555);
            serverSocket.bind(iSA);
            System.out.println ("Espera a que llegue una petición de socket");
            Socket socket = serverSocket.accept();
            System.out.println ("Se ha establecido la conexión");

            InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();
            /*
            Lectura de un array de bytes, la lectura más sencilla.
            */
            byte[] mensaje = new byte[14];//14 bytes, de 'PRIMER MENSAJE'
            is.read(mensaje);
            System.out.println("Mensaje:" + new String(mensaje));
            
            /*
            Lectura de líneas a través del BufferedReader, lectura de líneas
            completas (hasta fin de línea).
            */
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String linea = br.readLine();
            while (linea!=null){
                System.out.println("*" + linea + "*");
                linea = br.readLine();
            }
            
            System.out.println("Cerrando el socket de comunicación");
            socket.close();
            System.out.println("Cerrando el socket servidor");
            serverSocket.close();
        } catch (IOException ex) {
           ex.printStackTrace();
        }
        
	}
}
class IniciarCliente extends Thread {
	public void run() {
		System.out.println("Hola desde el hilo creado!");
		EnviarMensaje("mensaje");
	}

	private void EnviarMensaje(String string) {
        try {
            System.out.println("Creando socket cliente");
            Socket clientSocket = new Socket();
            System.out.println("Estableciendo conexión");
            InetSocketAddress addr = new InetSocketAddress("localhost",5555);
            clientSocket.connect(addr);
            
            InputStream is = clientSocket.getInputStream();
            OutputStream os = clientSocket.getOutputStream();
            
            System.out.println("Enviando mensajes");
            
            String mensaje="";
            do {
            	mensaje = Entrada.cadena();
            	if(mensaje.equals("fin")) {
            		return;
            	}else {
                    os.write(mensaje.getBytes());
            	}
            }while(mensaje.equals("fin"));
                        
            System.out.println("Mensaje enviado");
            System.out.println("Cerrando socket cliente");
            clientSocket.close();
            System.out.println("Terminado");
            
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
	}
}
