package ejemploSocketStream_Cliente_Servidor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import utilidades.Entrada;

public class Cliente {

    public static void main(String[] args) throws InterruptedException {
        try {
            System.out.println("Creando socket cliente");
            Socket clientSocket = new Socket();
            System.out.println("Estableciendo conexión");
            InetSocketAddress addr = new InetSocketAddress("localhost",52555);
            
            
            //InputStream is = clientSocket.getInputStream();

            //os.write("hola".getBytes());
            System.out.println("Enviando mensajes");
            
            String mensaje="";
            do {
            	mensaje = Entrada.cadena();
            	
            	if(mensaje.equals("fin")) {
                    System.out.println("Saliendo");
            		break;
            	}else {
            		clientSocket = new Socket();
            		addr = new InetSocketAddress("localhost",52555);
            		clientSocket.connect(addr);
                    OutputStream os = clientSocket.getOutputStream();
                    os.write(mensaje.getBytes());
                    os.close();
            	}
            	
            }while(!mensaje.equals("fin"));
                       
            System.out.println("Mensaje enviado");
            System.out.println("Cerrando socket cliente");
            
            clientSocket.close();
            System.out.println("Terminado");
            
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }
    
}
