package ejercicioEvaluable;

public class Cliente {

	private int[] productos;

	public Cliente(int[] productos) {
		this.productos = productos;
	}

	public int[] getProductos() {
		return productos;
	}

	public void setProductos(int[] productos) {
		this.productos = productos;
	}
}
