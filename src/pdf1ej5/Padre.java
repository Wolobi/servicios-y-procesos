package pdf1ej5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;


public class Padre {

	public static void main(String args[]) throws IOException {
		String arg1 = "java";
		String arg2 = "-jar";
		String hijoUno = "C:\\Users\\Alumno\\Desktop\\hijouno.jar";
		String hijoDos = "C:\\Users\\Alumno\\Desktop\\hijoDos.jar";

		// String[] param = {arg1,arg2,arg3,texto};
		String[] param = { arg1, arg2, hijoUno, "cmd", "/C", "dir" };
		Process process = new ProcessBuilder(param).start();
		InputStream is = (InputStream) process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		
		String[] param2 = { arg1, arg2, hijoDos, "cmd", "/C", "ipconfig" };
		Process process2 = new ProcessBuilder(param2).start();
		InputStream is2 = (InputStream) process2.getInputStream();
		InputStreamReader isr2 = new InputStreamReader(is2);
		BufferedReader br2 = new BufferedReader(isr2);
		
		OutputStream os = process2.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os);
		
		BufferedWriter bw = new BufferedWriter(osw);
		//PrintWriter pw = new PrintWriter(osw);
		
		String line;
		while ((line = br.readLine()) != null) {

			bw.write(line);
			bw.newLine();
			//pw.println(line);
		}
		bw.close();
		//pw.close();
		
		String line2;
		while ((line2 = br2.readLine()) != null) {
			System.out.println(line2);
		}
	}

}
