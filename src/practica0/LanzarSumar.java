package practica0;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class LanzarSumar {

	public static void main(String[] args) throws IOException {

		Process process = new ProcessBuilder(args).start();
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String linea;
		linea=br.readLine();
		System.out.println("Linea:"+linea);
		while ((linea=br.readLine())!=null) {
			System.out.println("resultado"+linea);
		}
		br.close();
	}

}
