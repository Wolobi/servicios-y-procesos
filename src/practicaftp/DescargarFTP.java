package practicaftp;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

public class DescargarFTP {

	public static void main(String[] args) throws SocketException, IOException {
		FTPClient cliente = new FTPClient();
		String servidor = "localhost";
		// String servidor = "ftp.rediris.es";
		System.out.println("Conecto con el servidor: " + servidor);
		// conectamos cliente FTP al servidor FTP
		cliente.connect(servidor);
		System.out.println(cliente.getReplyString());
		int respuesta = cliente.getReplyCode();
		if (!FTPReply.isPositiveCompletion(respuesta)) {
			System.out.println("Conexion NO ESTABLECIDA: " + respuesta);
			cliente.disconnect();
			System.exit(0);
		}
		cliente.login("mortadelo", "");

		respuesta = cliente.getReplyCode();
		if (!FTPReply.isPositiveCompletion(respuesta)) {
			System.out.println("Login no completado: " + respuesta);
			cliente.disconnect();
			System.exit(0);
		}

		boolean transform = cliente.setFileType(FTP.BINARY_FILE_TYPE);
		if (transform) {
			System.out.println("Tipo de transferencia cambiado a binario");
		} else {
			System.out.println("Intento de cambiar tipo de transferencia a binario fallido.");

		}

		FTPFile[] arrayFicheros = cliente.listFiles("/ftp");
		System.out.println(arrayFicheros.length);
		System.out.println();
		for (int i = 0; i < arrayFicheros.length; i++) {

			System.out.println(arrayFicheros[i].getName());

		}

		File file = new File("I:/descargaCompletada.png");
		// cliente.get
		OutputStream os = new BufferedOutputStream(new FileOutputStream(file));

		cliente.retrieveFile("/descarga.png", os);

		respuesta = cliente.getReplyCode();
		if (!FTPReply.isPositiveCompletion(respuesta)) {
			System.out.println("Archivo no descargado: " + respuesta);
			cliente.disconnect();
			System.exit(0);
		}

		os.close();
		cliente.logout();
		cliente.disconnect();

	}

}
