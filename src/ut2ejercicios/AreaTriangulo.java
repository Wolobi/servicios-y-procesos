package ut2ejercicios;

public class AreaTriangulo implements Runnable {
	
	double base;
	double altura;

	public AreaTriangulo(double base, double altura) {
		this.base = base;
		this.altura = altura;
		Thread th= new Thread(this);
		th.start();

	}

	@Override
	public void run() {
		System.out.println("Base: "+(int)base+" Altura: "+(int)altura+" Area: "+(base*altura)/2);
		
	}



}
