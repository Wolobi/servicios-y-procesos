package ut2ejercicios;

public class HiloC extends Thread {
	int c;

	public HiloC(int c) {
		this.c = c;
		System.out.println("Creando hilo: " + this.c);
	}

	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println("Hilo " + this.c + " línea " + (i + 1));
		}
	}
}
